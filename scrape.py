#!/usr/bin/env python3
from os import mkdir
from bs4 import BeautifulSoup
import requests
from urllib.request import urlopen
import re
import json

domain = 'https://www.schwaebisch-schwaetza.de'
base_url = f'{domain}/schwaebisches_woerterbuch.php'
start_url = base_url
url = start_url
get_param = ''

all_data = []
while True:
    #res = requests.get(url)
    #res = requests.get(url, data=get_param)
    #if res.status_code != 200:
    #    print(f'an error occured: {res.status_code}')
    #    breakpoint()
    #soup = BeautifulSoup(res.text, 'html.parser')
    res = urlopen(url)
    if res.status != 200:
        print(f'an error occured: {res.status_code}')
        breakpoint()
    soup = BeautifulSoup(res.read(), 'html.parser')
    content = soup.select('#inhalt_lex > div')
    for e in content:
        if ('id' not in e.attrs
                or not re.match('lexikon[a-z]+', e.attrs['id'])):
            continue

        audio, left, right, *rest = list(e.children)

        audio = audio.select('audio')
        if audio:
            if len(audio) > 1:
                print('missing audio')
            audio = audio[0].attrs['src']
        else:
            audio = None

        if len(list(left.children)) != 2:
            print(url)
            print(left)
            print()
            continue
        else:
            swabian, category = list(left.children)

        more = []
        if rest:
            for x in rest[1:]:
                more.append(x.text)

        data = {
            'german': right.text,
            'swabian': swabian.text,
            'audio': audio,
            'category': category.text,
            'more': more,
        }

        all_data.append(data)

    # next_button = soup.select_one('#inhalt_lex > table:nth-child(25) > tbody > tr > td > p > a:nth-child(7)')
    try:
        next_button = soup.select('#inhalt_lex > table > tr > td > small > a')[-2]
        assert next_button.text == '›'
        url = f'{base_url}{next_button.attrs["href"]}'
        #get_param = next_button.attrs["href"][1:]
    except Exception as e:
        print(e)
        break

print(json.dumps(all_data))
with open('data.json', 'w') as f:
    json.dump(all_data, f)

mkdir('audio')
sess = requests.Session()
for e in all_data:
    audio_path = e['audio']
    if not audio_path:
        continue
    url = f'{domain}/{audio_path}'
    res = sess.get(url)
    if res.status_code != 200:
        print(f'an error occured: {res.status_code}')
        breakpoint()
    assert re.match('audio/[^/]+.mp3$', audio_path)
    with open(audio_path, 'wb') as f:
        f.write(res.content)
